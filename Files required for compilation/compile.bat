set adt_directory=C:\Program Files (x86)\Adobe\Adobe Flash Builder 4.6\sdks\4.6.0\bin
set root_directory=LOCATION OF THIS PARTICULAR DIRECTORY IN YOUR FILE SYSTEM
set library_directory=%root_directory%\Library
set native_directory=%root_directory%\NativeAndroid
set signing_options=-storetype pkcs12 -keystore "CERTIFICATE_PATH" -storepass CERTIFICATE_PASSWORD
set dest_ANE=%root_directory%\Speech.ane
set extension_XML=%library_directory%\src\extension.xml
set library_SWC=%library_directory%\bin\SpeechLibrary.swc
"%adt_directory%"/adt -package %signing_options% -tsa none -target ane "%dest_ANE%" "%extension_XML%" -swc "%library_SWC%" -platform Android-ARM -C "%native_directory%" .