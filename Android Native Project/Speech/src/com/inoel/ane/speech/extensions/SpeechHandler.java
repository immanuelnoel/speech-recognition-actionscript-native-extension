/*
Copyright (c) 2012 http://www.immanuelnoel.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software or code, hereby referred to as 'Software' and associated documentation 
files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.inoel.ane.speech.extensions;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;

public class SpeechHandler extends Activity {
	
	public static SpeechExtensionContext speechContext;
	private static final int VOICE_RECOGNITION_REQUEST_CODE = 343222;
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 *
	 * Called when the activity is first created
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
        startActivityForResult(SpeechExtensionContext.speechIntent, VOICE_RECOGNITION_REQUEST_CODE);
    }
    
	/* (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 *
	 *  -> Listen for results from native speech utility
	 *  -> Dispatch events (dispatchStatusEventAsync) containing appropriate results. The library listens for these events. 
	 */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS); // Fill the list view with the strings the recognizer thought it could have heard
           
            SpeechListen.speechContext.dispatchStatusEventAsync("VOICE_RECOGNIZED",  (String)matches.get(0));
            finish();
        }
        else {
        	SpeechListen.speechContext.dispatchStatusEventAsync("VOICE_NOT_RECOGNIZED",  "Server or permission error. Please try again !"); //Return Error Code
            finish();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
