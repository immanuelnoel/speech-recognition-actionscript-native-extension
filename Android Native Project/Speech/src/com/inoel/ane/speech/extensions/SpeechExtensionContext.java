/*
Copyright (c) 2012 http://www.immanuelnoel.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software or code, hereby referred to as 'Software' and associated documentation 
files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.inoel.ane.speech.extensions;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;

public class SpeechExtensionContext extends FREContext {

	public static Intent speechIntent;
	
	@Override
	public void dispose() {
	}

	/*(non-Javadoc)
	 * @see com.adobe.fre.FREContext#getFunctions()
	 *
	 * Maps Exposed API names to internal classes.
	 */
	@Override
	public Map<String, FREFunction> getFunctions() {
		
		Map<String, FREFunction> map = new HashMap<String, FREFunction>();
		map.put("init", new SpeechInit()); // The Actionscript Library will call the API 'init', which in-turn initializes the SpeechInit class
		map.put("isSupported", new SpeechSupport());
		map.put("listen", new SpeechListen());
		
		return map;
	}

}
