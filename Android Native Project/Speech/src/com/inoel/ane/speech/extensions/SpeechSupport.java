/*
Copyright (c) 2012 http://www.immanuelnoel.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software or code, hereby referred to as 'Software' and associated documentation 
files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.inoel.ane.speech.extensions;

import java.util.List;

import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

public class SpeechSupport implements FREFunction {

	/* (non-Javadoc)
	 * @see com.adobe.fre.FREFunction#call(com.adobe.fre.FREContext, com.adobe.fre.FREObject[])
	 *
	 * Returns a boolean value to indicate speech utility's presence on the device
	 */
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		FREObject returnValue = null;
		
		try {
			returnValue = FREObject.newObject(false);
			
			// Check to see if a recognition activity is present
	        PackageManager pm = context.getActivity().getPackageManager();
	        List<ResolveInfo> activities = pm.queryIntentActivities(SpeechExtensionContext.speechIntent, 0);
	        if (activities.size() != 0) {
	        	returnValue = FREObject.newObject(true);
	        } else {
	        	returnValue = FREObject.newObject(false);
	        }
		} catch (FREWrongThreadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
	}

}
