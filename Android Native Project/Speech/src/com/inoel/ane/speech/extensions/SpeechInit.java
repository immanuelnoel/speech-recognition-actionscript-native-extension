/*
Copyright (c) 2012 http://www.immanuelnoel.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software or code, hereby referred to as 'Software' and associated documentation 
files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.inoel.ane.speech.extensions;

import android.content.Intent;
import android.speech.RecognizerIntent;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;

public class SpeechInit implements FREFunction {

	/* (non-Javadoc)
	 * @see com.adobe.fre.FREFunction#call(com.adobe.fre.FREContext, com.adobe.fre.FREObject[])
	 *
	 * Initializes the 'SpeechExtensionContext.speechIntent' intent
	 */
	@Override
	public FREObject call(FREContext context, FREObject[] args) {
		
		String extraPrompt = "";
		FREObject freObj = args[0];
		try {
			extraPrompt = freObj.getAsString();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FRETypeMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FREInvalidObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FREWrongThreadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SpeechExtensionContext.speechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		SpeechExtensionContext.speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM); // See Android documentation for other options
		SpeechExtensionContext.speechIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, extraPrompt); // Custom text to be displayed on the native Speech Input UI
		SpeechExtensionContext.speechIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,1); // Indicates number of results returned is always ONE
		
		return null;
	}

}
