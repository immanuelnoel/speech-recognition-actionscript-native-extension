/*
Copyright (c) 2012 http://www.immanuelnoel.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software or code, hereby referred to as 'Software' and associated documentation 
files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.inoel.ane.speech
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.StatusEvent;
	import flash.external.ExtensionContext;
	
	import mx.core.FlexGlobals;
	
	public class Speech extends EventDispatcher
	{
		private static var extContext:ExtensionContext;
		
		/**
		 * Constructor
		 */
		public function Speech(customPrompt:String = "")
		{
			super();
			
			if(!extContext)
			{
				extContext = ExtensionContext.createExtensionContext("com.inoel.ane.speech", "Speech");
				
				/**
				 * 'Init' class in Native Project accepts a customPrompt as a parameter. 
				 * This is displayed on the Native Speech Recognition UI
				 */
				extContext.call("init", customPrompt);
			}
		}
		
		/**
		 * Method to check if speech input feature is supported. 
		 * Returns a Boolean
		 */
		public function get isSupported():Boolean
		{
			return extContext.call("isSupported");
		}
		
		/**
		 * Launch's the Native Speech Recognition UI for accepting speech input
		 */
		public function listen():void
		{
			extContext.addEventListener(StatusEvent.STATUS, reDispatchEvent); //Listen for StatusEvent - Fired from the native code
			
			extContext.call("listen");
		}
		
		/**
		 * Listen to events dispatched from Native code on successful / unsuccessful interpretation of voice input. 
		 */
		protected function reDispatchEvent(event:StatusEvent):void
		{
			if(event.code == "VOICE_RECOGNIZED")
			{
				trace(event.level);
				this.dispatchEvent(new speechToTextEvent(speechToTextEvent.VOICE_RECOGNIZED, event.level));
			}
			else
			{
				trace(event.level);
				this.dispatchEvent(new speechToTextEvent(speechToTextEvent.VOICE_NOT_RECOGNIZED, event.level));
			}
		}
	}
}